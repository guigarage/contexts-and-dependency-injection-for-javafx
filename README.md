It is planned to define a default CDI API for JavaFX with this project. At the moment some JavaFX based frameworks like DataFX (http://www.javafxdata.org), afterburner.fx (http://afterburner.adam-bien.com) or mvvmFX (https://github.com/sialcasa/mvvmFX) try to implement Injection or CDI for JavaFX. With this project a set of default interfaces and classes should be defined that can be used by all these frameworks.

The CDI specification for JavaEE is defined by this artifact:

<dependency>
			<groupId>javax.enterprise</groupId>
			<artifactId>cdi-api</artifactId>
			<version>1.1</version>
</dependency>